//
//  VideoDadosTests.swift
//  teste-mobileTests
//
//  Created by Marcio Izar Bastos de Oliveira on 06/10/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import XCTest
@testable import teste_mobile

class VideoDadosTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDeveCarregarListaDeVideos() {
        let expectation2 = expectation(description: "foobar")
        let videoDados = VideoDados(networking: MockNetworking())
        videoDados.getListaVideos(textoPesquisa: "x", listaVideos: nil) { videos in
            DispatchQueue.main.async {
                XCTAssertEqual(5, videos?.items.count)
                XCTAssertEqual("Palavra Cantada | Rato", videos?.items[0].snippet.title)
                XCTAssertEqual("FYNk1HMi5d4", videos?.items[1].id.videoId)
                XCTAssertEqual("https://yt3.ggpht.com/a/AATXAJyjYPBBh5-g3SX0o3yECPJexEe5bkL73TA8JZIVYA=s240-c-k-c0xffffffff-no-rj-mo", videos?.items[2].snippet.thumbnails.medium.url)
                XCTAssertEqual("Como fazer uma armadilha, com um balde e uma bexiga para pegar dezenas de ratos de uma vez só #abcde", videos?.items[4].snippet.title)
            }
            }
        expectation2.fulfill()
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    
    func testDeveMostrarDetalhesDoVideo() {
        let expectation2 = expectation(description: "foobar")
        let videoDados = VideoDados(networking: MockNetworking())
        videoDados.getVideo(idVideo: "x") { video in
            DispatchQueue.main.async {
                XCTAssertEqual("dKS5JIdq7IA", video?.id)
                XCTAssertEqual("Aquário Virtual - Flautas Indígenas e Musica Calma (Virtual Aquarium)", video?.snippet.title)
            }
        }
        expectation2.fulfill()
        waitForExpectations(timeout: 5, handler: nil)
    }
    
}
