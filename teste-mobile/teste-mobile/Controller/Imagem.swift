//
//  Imagem.swift
//  teste-mobile
//
//  Created by Marcio Izar Bastos de Oliveira on 30/09/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import UIKit
import AVFoundation

class Imagem {
    var url: URL
    
    init(url: URL) {
        self.url = url
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(completion: @escaping ((_ image: UIImage?)->Void)) {
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            DispatchQueue.main.async {
                completion(UIImage(data: data))
            }
        }
    }
}

