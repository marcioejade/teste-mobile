//
//  ViewController.swift
//  teste-mobile
//
//  Created by Marcio Izar Bastos de Oliveira on 17/09/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UINavigationControllerDelegate {

    @IBOutlet weak var madeinwebImageView: UIImageView!
    @IBOutlet weak var buscaTextField: UITextField!
    @IBOutlet weak var buscarButton: UIButton!
    @IBOutlet weak var textoAlerta: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupLayout()
        navigationController?.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    private func setupLayout() {
        textoAlerta.alpha = 0.0
        
        buscaTextField.backgroundColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 0.30)
        buscaTextField.layer.borderWidth = 1
        buscaTextField.layer.borderColor = UIColor(red: 0.59, green: 0.59, blue: 0.59, alpha: 1.00).cgColor
        buscaTextField.textColor = UIColor(red: 155/255.0, green: 155/255.0, blue:155/255.0, alpha:1/1.0)
        buscaTextField.setLeftPaddingPoints(16.9)
        buscaTextField.font = UIFont(name: "Helvetica", size: 16)

        buscarButton.backgroundColor = UIColor(red: 0.13, green: 0.48, blue: 0.88, alpha: 1.00)
        buscarButton.layer.borderWidth = 1
        buscarButton.layer.borderColor = UIColor(red: 0.59, green: 0.59, blue: 0.59, alpha: 1.00).cgColor
        buscarButton.tintColor = .white
        buscarButton.titleLabel!.font = UIFont(name: "Helvetica-Bold", size: 16)
    }

    @IBAction func buscarVideos(_ sender: Any) {
        show(textoPesquisa: buscaTextField.text ?? "")
    }
    
    func show(textoPesquisa: String) {
        if textoPesquisa == "" {
            UIView.animate(withDuration: 0.5) {
                self.textoAlerta.alpha = 1.0
            }
        } else {
            UIView.animate(withDuration: 0.2) {
                self.textoAlerta.alpha = 0.0
            }
            guard let vc = storyboard?.instantiateViewController(identifier: "ListaVideoViewController", creator: { coder in
                return ListaVideoViewController(coder: coder, textoPesquisa: textoPesquisa)
            }) else {
                fatalError("Problemas em carregar ListaVideoViewController do storyboard.")
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
        
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        let diversosTiposDeViews: [UIView] = [madeinwebImageView, buscarButton, buscaTextField]

        switch operation {
        case .push:
            return AnimacaoTransicaoPersonalizada(duracao: 1.0, views: diversosTiposDeViews, apresentarViewController: true)
        default:
            return AnimacaoTransicaoPersonalizada(duracao: 1.0, views: diversosTiposDeViews, apresentarViewController: false)
        }
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}




