//
//  ExibeVideoViewController.swift
//  teste-mobile
//
//  Created by Marcio Izar Bastos de Oliveira on 22/09/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import UIKit

class ExibeVideoViewController: UIViewController {
    
    var videoId: String = ""
    @IBOutlet weak var cometa: UIImageView!
    
    init?(coder: NSCoder, videoId: String) {
        self.videoId = videoId
        super.init(coder: coder)
    }

    required init?(coder: NSCoder) {
        fatalError("É necessário passar textoPesquisa para criar este View Controller.")
    }


    override func viewDidLoad() {
        super.viewDidLoad()
//        self.modalPresentationStyle = .fullScreen
        
//        navigationController?.delegate = self

        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(imageTapped2(tapGestureRecognizer:)))
        self.cometa.isUserInteractionEnabled = true
        self.cometa.addGestureRecognizer(tapGestureRecognizer2)
    }
    

    @objc func imageTapped2(tapGestureRecognizer: UITapGestureRecognizer) {
        print("oi")
        self.dismiss(animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }

}
