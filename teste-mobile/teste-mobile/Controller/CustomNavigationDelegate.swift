//
//  CustomNavigationDelegate.swift
//  teste-mobile
//
//  Created by Marcio Izar Bastos de Oliveira on 23/09/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

//import UIKit
//
//class CustomNavigationDelegate: NSObject, UINavigationControllerDelegate {
//
//    var interactionController: UIPercentDrivenInteractiveTransition?
//    var current: UIViewController?
//    var pushDestination: (() -> UIViewController?)?
//
//    func navigationController(_ navigationController: UINavigationController,
//                              animationControllerFor operation: UINavigationController.Operation,
//                              from fromVC: UIViewController,
//                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        return CustomNavigationAnimator(transitionType: operation)
//    }
//
//    func navigationController(_ navigationController: UINavigationController,
//                              interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
//        return interactionController
//    }
//
//    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
//        current = viewController
//    }
//}
