//
//  ListaVideoViewController.swift
//  teste-mobile
//
//  Created by Marcio Izar Bastos de Oliveira on 18/09/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import UIKit
import AVFoundation

class ListaVideoViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIViewControllerTransitioningDelegate {
    
    var textoPesquisa: String = ""

    init?(coder: NSCoder, textoPesquisa: String) {
        self.textoPesquisa = textoPesquisa
        super.init(coder: coder)
    }

    required init?(coder: NSCoder) {
        fatalError("É necessário passar textoPesquisa para criar este View Controller.")
    }
    
    @IBOutlet weak var madeinwebImageView: UIImageView!
    @IBOutlet weak var buscaTextField: UITextField!
    @IBOutlet weak var buscarButton: UIButton!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var videosTableView: UITableView!
    @IBOutlet weak var alertaLabel: UILabel!
    @IBOutlet weak var loadingProgressView: UIProgressView!
    
    var listaVideos: ListaVideosSearch?
    var videoSelecionado: VideoSearch?
    var imagemSelecionada: UIImage?
    var frameViews: [CGRect]?
    var currentCell: ListaVideoTableViewCell?
    var views: [UIView]?
    let videoDados = VideoDados(networking: HTTPNetworking())
    var carregou = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        realizarPesquisa(texto: textoPesquisa)
    }
    
    private func realizarPesquisa(texto: String) {
        loadingProgressView.isHidden = false
        self.loadingProgressView.setProgress(0.1, animated: false)
        UIView.animate(withDuration: 0.5) {
            self.loadingProgressView.setProgress(0.2, animated: true)
        }
        videoDados.getListaVideos(textoPesquisa: texto, listaVideos: nil) { videos in
            self.listaVideos = videos
            DispatchQueue.main.async {
                if self.listaVideos == nil {
                    UIView.animate(withDuration: 0.5) {
                        self.alertaLabel.alpha = 1.0
                    }
                } else {
                    self.alertaLabel.alpha = 0
                    self.videosTableView.reloadData()
                }
                UIView.animate(withDuration: 5.0) {
                    self.loadingProgressView.isHidden = true
                }
                
            }
        }
        UIView.animate(withDuration: 0.5) {
            self.loadingProgressView.setProgress(0.6, animated: true)
        }
        UIView.animate(withDuration: 0.5) {
            self.loadingProgressView.setProgress(1.0, animated: true)
        }
        self.textoPesquisa = texto
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    private func setupLayout() {
        alertaLabel.alpha = 0
        
        buscaTextField.backgroundColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 0.30)
        buscaTextField.layer.borderWidth = 1
        buscaTextField.layer.borderColor = UIColor(red: 0.59, green: 0.59, blue: 0.59, alpha: 1.00).cgColor
        buscaTextField.textColor = UIColor(red: 155/255.0, green: 155/255.0, blue:155/255.0, alpha:1/1.0)
        buscaTextField.setLeftPaddingPoints(17.5)
        buscaTextField.font = UIFont(name: "Helvetica", size: 16)
        buscaTextField.text = textoPesquisa

        buscarButton.backgroundColor = UIColor(red: 0.13, green: 0.48, blue: 0.88, alpha: 1.00)
        buscarButton.layer.borderWidth = 1
        buscarButton.layer.borderColor = UIColor(red: 0.59, green: 0.59, blue: 0.59, alpha: 1.00).cgColor
        buscarButton.tintColor = .white
        buscarButton.titleLabel!.font = UIFont(name: "Helvetica-Bold", size: 14)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        madeinwebImageView.isUserInteractionEnabled = true
        madeinwebImageView.addGestureRecognizer(tapGestureRecognizer)
        
        viewHeader.backgroundColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 0.30)
        let cinza = UIColor(red: 0.59, green: 0.59, blue: 0.59, alpha: 1.00)
        viewHeader.addBottomBorderWithColor(color: cinza, width: 1)

        videosTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaVideos?.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ListaVideoTableViewCell
        
        if let strUrl = listaVideos?.items[indexPath.row].snippet.thumbnails.medium.url {
            if let url = URL(string: strUrl) {
                let imagem = Imagem(url: url)
                imagem.downloadImage() { thumbImage in
                    cell.thumbImageView.image = thumbImage
                }
            }
        }
        cell.titulo.text = listaVideos?.items[indexPath.row].snippet.title
        cell.titulo.font = UIFont(name: "Helvetica-Bold", size: 16)
        cell.titulo.textColor = UIColor(red: 0.13, green:0.48, blue:0.88, alpha:1.00)
        
        cell.descricao.text = listaVideos?.items[indexPath.row].snippet.description
        cell.descricao.font = UIFont(name: "Helvetica-Light", size: 14)
        cell.descricao.textColor = UIColor(red: 0.43, green:0.43, blue:0.43, alpha:1.00)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 127
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // calculates where the user is in the y-axis
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height

        if offsetY >= contentHeight - scrollView.frame.size.height {
            if carregou {
                carregou = false
                videoDados.getListaVideos(textoPesquisa: textoPesquisa, listaVideos: listaVideos) { videos in
                    if self.listaVideos?.items.count != videos?.items.count {
                        self.listaVideos = videos
                        DispatchQueue.main.async {
                            self.videosTableView.reloadData()
                        }
                        self.carregou = true
                    }
                }
            }
        }
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        videoSelecionado = listaVideos?.items[indexPath.row]
        
        guard let vc = storyboard?.instantiateViewController(identifier: "VideoViewController", creator: { coder in
            return VideoViewController(coder: coder, video: self.videoSelecionado!)
        }) else {
            fatalError("Problemas em carregar VideoViewController do storyboard.")
        }
        
        currentCell = tableView.cellForRow(at: indexPath) as? ListaVideoTableViewCell
        views = [currentCell!.titulo, currentCell!.descricao, currentCell!.thumbImageView]
        imagemSelecionada = currentCell!.thumbImageView.image
        
        let frameSelecionado = tableView.rectForRow(at: indexPath)
        let tituloFrame = CGRect(x: currentCell!.titulo.frame.minX, y: currentCell!.titulo.frame.midY + 2 + frameSelecionado.minY + tableView.frame.minY - videosTableView.contentOffset.y, width: currentCell!.titulo.frame.width, height: currentCell!.titulo.frame.height)
        let descricaoFrame = CGRect(x: currentCell!.descricao.frame.minX, y: currentCell!.descricao.frame.minY + frameSelecionado.minY + tableView.frame.minY + 4 - videosTableView.contentOffset.y, width: currentCell!.descricao.frame.width, height: currentCell!.descricao.frame.height)
        let imagemFrame = CGRect(x: currentCell!.thumbImageView.frame.minX, y: currentCell!.thumbImageView.frame.minY + 12 + frameSelecionado.minY + tableView.frame.minY - videosTableView.contentOffset.y, width: currentCell!.thumbImageView.frame.width, height: currentCell!.thumbImageView.frame.height)
        
        frameViews = [tituloFrame, descricaoFrame, imagemFrame]
        
        vc.modalPresentationStyle = .fullScreen
        vc.transitioningDelegate = self

        present(vc, animated: true, completion: nil)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AnimacaoTransicaoPersonalizada2(duracao: 1.0, imagem: imagemSelecionada, frames: frameViews!, views: views!, apresentarViewController: true)
    }

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return AnimacaoTransicaoPersonalizada2(duracao: 1.0, imagem: imagemSelecionada, frames: frameViews!, views: views!, apresentarViewController: false)
    }
    
    @IBAction func tocarBuscarButton(_ sender: Any) {
        if buscaTextField.text != nil && buscaTextField.text != "" && buscaTextField.text != textoPesquisa {
            realizarPesquisa(texto: buscaTextField.text!)
        }
    }
}

extension UIView {
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor

        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width: UIScreen.main.bounds.width + UIScreen.main.bounds.height, height: width)
        self.layer.addSublayer(border)
    }
}
