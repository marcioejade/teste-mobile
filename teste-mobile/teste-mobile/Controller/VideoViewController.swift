//
//  VideoViewController.swift
//  teste-mobile
//
//  Created by Marcio Izar Bastos de Oliveira on 22/09/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView

class VideoViewController: UIViewController, UIViewControllerTransitioningDelegate {
    
    var videoSearch: VideoSearch
    var video: Video?

    init?(coder: NSCoder, video: VideoSearch) {
        self.videoSearch = video
        super.init(coder: coder)
    }

    required init?(coder: NSCoder) {
        fatalError("É necessário passar textoPesquisa para criar este View Controller.")
    }
    @IBOutlet weak var setaVoltarImageView: UIImageView!
    @IBOutlet weak var madeinwebImageView: UIImageView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var voltarView: UIView!
    @IBOutlet weak var visualizacoesLabel: UILabel!
    @IBOutlet weak var descricaoTextView: UITextView!
    @IBOutlet weak var voltarButton: UIButton!
    @IBOutlet weak var textoAlerta: UILabel!
    @IBOutlet weak var playerView: WKYTPlayerView!
    
    var imagemSelecionada: UIImage?
    var frameViews: [CGRect]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        chamaDetalhesVideo()
    }
    
    private func chamaDetalhesVideo() {
        video = Video(id: videoSearch.id.videoId, snippet: videoSearch.snippet, statistics: nil)
        textoAlerta.alpha = 0.0
        
        let videoDados = VideoDados(networking: HTTPNetworking() as Networking)
        videoDados.getVideo(idVideo: videoSearch.id.videoId) { video in
            if video != nil {
//                self.textoAlerta.alpha = 0.0
                self.video = video!
            } else {
                UIView.animate(withDuration: 0.5) {
                    self.textoAlerta.alpha = 1.0
                }
            }
            DispatchQueue.main.async {
                self.setupLayout()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    private func setupLayout() {
        viewHeader.backgroundColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 0.30)
        
        voltarView.backgroundColor = .clear
        let tap = UITapGestureRecognizer(target: self, action:#selector(imageTapped(tapGestureRecognizer:)))
        voltarView.addGestureRecognizer(tap)
        
        voltarButton.backgroundColor = .clear
        voltarButton.layer.borderWidth = 0
        voltarButton.layer.borderColor = UIColor.clear.cgColor
        voltarButton.tintColor = UIColor(red: 0.71, green: 0.71, blue:0.71, alpha:1.0)
        voltarButton.titleLabel!.font = UIFont(name: "Helvetica", size: 14)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        setaVoltarImageView.isUserInteractionEnabled = true
        setaVoltarImageView.addGestureRecognizer(tapGestureRecognizer)
        
        tituloLabel.textColor = UIColor(red: 0.13, green: 0.48, blue:0.88, alpha:1.0)
        tituloLabel.font = UIFont(name: "Helvetica-Bold", size: 20)
        if let video = video?.snippet.title {
            tituloLabel.text = video
        }

        visualizacoesLabel.textColor = UIColor(red: 0.65, green: 0.65, blue:0.65, alpha:1.0)
        visualizacoesLabel.font = UIFont(name: "Helvetica-Bold", size: 14)
        visualizacoesLabel.text = ""
        if let statistics = video?.statistics {
            if let visualizacoes = Int(statistics.viewCount) {
                let formatter = NumberFormatter()
                formatter.numberStyle = .decimal
                formatter.maximumFractionDigits = 0
                formatter.groupingSeparator = "."
                
                visualizacoesLabel.text = "\(formatter.string(for: visualizacoes) ?? "0") visualizações"
            }
        }
        
        descricaoTextView.backgroundColor = .clear
        descricaoTextView.textColor = UIColor(red: 0.43, green: 0.43, blue:0.43, alpha:1.0)
        descricaoTextView.font = UIFont(name: "Helvetica", size: 14)
        if let video = video?.snippet.description {
            descricaoTextView.text = video
        }
        
        if let video = video?.id {
            print(video)
            playerView.load(withVideoId: video)
        }
    }
    
    @IBAction func voltarTapped(_ sender: Any) {
        voltaVC()
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        voltaVC()
    }
    
    private func voltaVC() {
        self.dismiss(animated: true, completion: nil)
    }
}
