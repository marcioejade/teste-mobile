//
//  ListaVideos.swift
//  teste-mobile
//
//  Created by Marcio Izar Bastos de Oliveira on 30/09/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import Foundation

struct ListaVideosSearch: Codable {
    var pageInfo: PageInfo?
    var items: [VideoSearch]
    var nextPageToken: String?
}

struct ListaVideos: Codable {
    var items: [Video]
}

struct PageInfo: Codable {
    var totalResults: Int
    var resultsPerPage: Int
}
