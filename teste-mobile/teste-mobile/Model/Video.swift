//
//  Video.swift
//  teste-mobile
//
//  Created by Marcio Izar Bastos de Oliveira on 24/09/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import Foundation

struct VideoSearch: Codable {
    var id: Id
    var snippet: Snippet
}

struct Video: Codable {
    var id: String
    var snippet: Snippet
    var statistics: Statistics?
}

struct Id: Codable {
    var videoId: String
}

struct Snippet: Codable {
    var title: String
    var description: String
    var thumbnails: Thumbnails
}

struct Thumbnails: Codable {
    var medium: Medium
}

struct Medium: Codable {
    var url: String
}

struct Statistics: Codable {
    var viewCount: String
}
