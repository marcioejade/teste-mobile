//
//  VideoDados.swift
//  teste-mobile
//
//  Created by Marcio Izar Bastos de Oliveira on 29/09/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import Foundation

class VideoDados {
    var networking: Networking
    
    init(networking: Networking) {
        self.networking = networking
    }
    
    func getListaVideos(textoPesquisa: String, listaVideos: ListaVideosSearch?, completion: @escaping (ListaVideosSearch?) -> ()) {
        var parametro = ["":""]
        if listaVideos != nil {
            if listaVideos!.nextPageToken == nil {
                completion(listaVideos)
                return
            }
            parametro = ["q":textoPesquisa, "pageToken":listaVideos!.nextPageToken!]
        } else {
            parametro = ["q":textoPesquisa]
        }
        networking.request(from: Youtube.listaVideos, parameters: parametro) { data, error in
            guard let dataResponse = data, error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                completion(nil)
                return
            }
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                //Decode JSON Response Data
                var model = try decoder.decode(ListaVideosSearch.self, from: dataResponse)
                if listaVideos != nil {
                    var listaVideos2 = listaVideos
                    listaVideos2?.nextPageToken = model.nextPageToken
                    listaVideos2!.items.append(contentsOf: model.items)
                    model = listaVideos2!
                }
                completion(model)
            } catch let parsingError {
                print("Erro Desconhecido - ", parsingError)
                print("JSON String: \(String(data: dataResponse, encoding: .utf8) ?? "")")
            }
        }
    }
    
    func getVideo(idVideo: String, completion: @escaping (Video?) -> ()) {
        networking.request(from: Youtube.detalhesVideo, parameters: ["id":idVideo]) { data, error in
            guard let dataResponse = data, error == nil else {
                print(error?.localizedDescription ?? "Response Error")
                completion(nil)
                return
            }
            do {
                //here dataResponse received from a network request
                let decoder = JSONDecoder()
                //Decode JSON Response Data
                let model = try decoder.decode(ListaVideos.self, from: dataResponse)
                completion(model.items[0])
            } catch let parsingError {
                        print("Erro Desconhecido - ", parsingError)
                print("JSON String: \(String(data: dataResponse, encoding: .utf8) ?? "")")
            }
        }
    }
}
