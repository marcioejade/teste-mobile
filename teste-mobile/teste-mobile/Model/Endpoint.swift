//
//  Endpoint.swift
//  teste-mobile
//
//  Created by Marcio Izar Bastos de Oliveira on 07/10/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import Foundation

protocol Endpoint {
    var path: String { get }
}

enum Youtube {
    case listaVideos
    case detalhesVideo
}

extension Youtube: Endpoint {
    var path: String {
        switch self {
            case .listaVideos: return "https://www.googleapis.com/youtube/v3/search?part=id,snippet&key=AIzaSyAddzrzKVSE04xFauWKj7B-L-gDZEECqKM&type=video&maxResults=10"
            case .detalhesVideo: return "https://www.googleapis.com/youtube/v3/videos?part=snippet,statistics&key=AIzaSyAddzrzKVSE04xFauWKj7B-L-gDZEECqKM"
        }
    }
    var mock: String {
        switch self {
            case .listaVideos: return "ListaVideos"
            case .detalhesVideo: return "VideoDetalhes"
        }
    }
}
