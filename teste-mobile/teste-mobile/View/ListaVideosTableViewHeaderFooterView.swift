//
//  ListaVideosTableViewHeaderFooterView.swift
//  teste-mobile
//
//  Created by Marcio Izar Bastos de Oliveira on 05/10/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import UIKit

class ListaVideosTableViewHeaderFooterView: UITableViewHeaderFooterView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    let anterior = UILabel()
    let proximo = UILabel()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        configureContents()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configureContents() {
        anterior.translatesAutoresizingMaskIntoConstraints = false
        proximo.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(anterior)
        contentView.addSubview(proximo)

        // Center the image vertically and place it near the leading
        // edge of the view. Constrain its width and height to 50 points.
        NSLayoutConstraint.activate([
            anterior.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            anterior.widthAnchor.constraint(equalToConstant: 50),
            anterior.heightAnchor.constraint(equalToConstant: 50),
            anterior.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        
            // Center the label vertically, and use it to fill the remaining
            // space in the header view.
            proximo.heightAnchor.constraint(equalToConstant: 30),
            proximo.leadingAnchor.constraint(equalTo: anterior.trailingAnchor,
                   constant: 8),
            proximo.trailingAnchor.constraint(equalTo:
                   contentView.layoutMarginsGuide.trailingAnchor),
            proximo.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
    

}
