//
//  AnimacaoTransicaoPersonalizada.swift
//  teste-mobile
//
//  Created by Marcio Izar Bastos de Oliveira on 18/09/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import UIKit

class AnimacaoTransicaoPersonalizada: NSObject, UIViewControllerAnimatedTransitioning {
    
    // MARK: - Atributos
    
    private var duracao: TimeInterval
    private var views: [UIView]
    private var apresentarViewController: Bool
    
    // MARK: - Inicializador
    
    init(duracao: TimeInterval, views: [UIView], apresentarViewController: Bool) {
        self.duracao = duracao
        self.views = views
        self.apresentarViewController = apresentarViewController
    }
    
    // MARK: - Metodos
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duracao
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let viewInicial = transitionContext.view(forKey: UITransitionContextViewKey.from) else { return }
        guard let viewFinal = transitionContext.view(forKey: UITransitionContextViewKey.to) else { return }
        let imagem = views[0] as! UIImageView
        let botao = views[1] as! UIButton
        let textField = views[2] as! UITextField

        let contexto = transitionContext.containerView

        if apresentarViewController {
            contexto.addSubview(viewFinal)
        } else {
            contexto.insertSubview(viewFinal, belowSubview: viewInicial)
        }
        let viewAtual = apresentarViewController ? viewFinal : viewInicial
        
        guard let imagemDePreTransicao = viewAtual.viewWithTag(1) as? UIImageView else { return }
        imagemDePreTransicao.image = imagem.image
        guard let botaoDePreTransicao = viewAtual.viewWithTag(2) as? UIButton else { return }
        guard let textFieldDePreTransicao = viewAtual.viewWithTag(3) as? UITextField else { return }
        
        imagem.isHidden = true
        botao.isHidden = true
        textField.isHidden = true
        imagemDePreTransicao.isHidden = true
        botaoDePreTransicao.isHidden = true
        textFieldDePreTransicao.isHidden = true

        let imagemDeTransicao = apresentarViewController ? UIImageView(frame: imagem.frame) : UIImageView(frame: CGRect(x: imagemDePreTransicao.frame.minX, y: imagemDePreTransicao.frame.midY + 7, width: imagemDePreTransicao.frame.width, height: imagemDePreTransicao.frame.height))
        imagemDeTransicao.image = imagem.image
        
        let botaoDeTransicao = apresentarViewController ? UIButton(frame: botao.frame) : UIButton(frame: CGRect(x: botaoDePreTransicao.frame.minX, y: botaoDePreTransicao.frame.midY, width: botaoDePreTransicao.frame.width, height: botaoDePreTransicao.frame.height))
        
        botaoDeTransicao.backgroundColor = UIColor(red: 0.13, green: 0.48, blue: 0.88, alpha: 1.00)
        botaoDeTransicao.layer.borderWidth = 1
        botaoDeTransicao.layer.borderColor = UIColor(red: 0.59, green: 0.59, blue: 0.59, alpha: 1.00).cgColor
        botaoDeTransicao.tintColor = .white
        botaoDeTransicao.titleLabel!.font = UIFont(name: "Helvetica-Bold", size: 16)
        
        botaoDeTransicao.setTitleWithoutAnimation(title: "Buscar")
        
        let textFieldDeTransicao = apresentarViewController ? UITextField(frame: textField.frame) : UITextField(frame: CGRect(x: textFieldDePreTransicao.frame.minX, y: textFieldDePreTransicao.frame.midY, width: textFieldDePreTransicao.frame.width, height: textFieldDePreTransicao.frame.height))
        
        textFieldDeTransicao.backgroundColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 0.30)
        textFieldDeTransicao.layer.borderWidth = 1
        textFieldDeTransicao.layer.borderColor = UIColor(red: 0.59, green: 0.59, blue: 0.59, alpha: 1.00).cgColor
        textFieldDeTransicao.textColor = UIColor(red: 155/255.0, green: 155/255.0, blue:155/255.0, alpha:1/1.0)
        textFieldDeTransicao.setLeftPaddingPoints(16.9)
        textFieldDeTransicao.font = UIFont(name: "Helvetica", size: 16)
        
        textFieldDeTransicao.placeholder = textField.text

        contexto.addSubview(imagemDeTransicao)
        contexto.addSubview(botaoDeTransicao)
        contexto.addSubview(textFieldDeTransicao)

        if apresentarViewController {
            viewAtual.alpha = 0.0
        } else {
            viewInicial.alpha = 1.0
        }
        
        viewAtual.layoutIfNeeded()
        
        UIView.animate(withDuration: duracao, animations: {
            imagemDeTransicao.frame = self.apresentarViewController ? CGRect(x: imagemDePreTransicao.frame.minX, y: imagemDePreTransicao.frame.midY + 7, width: imagemDePreTransicao.frame.width, height: imagemDePreTransicao.frame.height) : imagem.frame
            botaoDeTransicao.frame = self.apresentarViewController ? CGRect(x: botaoDePreTransicao.frame.minX, y: botaoDePreTransicao.frame.midY, width: botaoDePreTransicao.frame.width, height: botaoDePreTransicao.frame.height) : botao.frame
            textFieldDeTransicao.frame = self.apresentarViewController ? CGRect(x: textFieldDePreTransicao.frame.minX, y: textFieldDePreTransicao.frame.midY, width: textFieldDePreTransicao.frame.width, height: textFieldDePreTransicao.frame.height) : textField.frame
            
            if self.apresentarViewController {
                viewAtual.alpha = 1.0
            } else {
                viewInicial.alpha = 0.0
            }

        }) { (_ ) in
            imagemDeTransicao.removeFromSuperview()
            botaoDeTransicao.removeFromSuperview()
            textFieldDeTransicao.removeFromSuperview()

            imagem.isHidden = self.apresentarViewController ? true : false
            botao.isHidden = self.apresentarViewController ? true : false
            textField.isHidden = self.apresentarViewController ? true : false
            imagemDePreTransicao.isHidden = false
            botaoDePreTransicao.isHidden = false
            textFieldDePreTransicao.isHidden = false
            
            transitionContext.completeTransition(true)
        }
    }
}

extension UIButton {
    func setTitleWithoutAnimation(title: String?) {
        UIView.setAnimationsEnabled(false)

        setTitle(title, for: .normal)
        titleLabel!.font = UIFont(name: "Helvetica-Bold", size: 15)

        layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
}
