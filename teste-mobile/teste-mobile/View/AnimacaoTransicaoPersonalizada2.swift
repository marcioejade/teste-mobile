//
//  AnimacaoTransicaoPersonalizada2.swift
//  teste-mobile
//
//  Created by Marcio Izar Bastos de Oliveira on 24/09/20.
//  Copyright © 2020 Marcio Izar Bastos de Oliveira. All rights reserved.
//

import UIKit

class AnimacaoTransicaoPersonalizada2: NSObject, UIViewControllerAnimatedTransitioning {
    
    // MARK: - Atributos
    
    private var duracao: TimeInterval
    private var imagem: UIImage?
    private var frames: [CGRect]
    private var views: [UIView]
//    private var textos: [String]
    private var apresentarViewController: Bool
    
    // MARK: - Inicializador
    
//    init(duracao: TimeInterval, imagem: UIImage?, frames: [CGRect], textos: [String], apresentarViewController: Bool) {
    init(duracao: TimeInterval, imagem: UIImage?, frames: [CGRect], views: [UIView], apresentarViewController: Bool) {
        self.duracao = duracao
        self.imagem = imagem
        self.frames = frames
        self.views = views
//        self.textos = textos
        self.apresentarViewController = apresentarViewController
    }
    
    // MARK: - Metodos
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duracao
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let viewInicial = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from) else { return }
        guard let viewFinal = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) else { return }

        let contexto = transitionContext.containerView

        if apresentarViewController {
            contexto.addSubview(viewFinal.view)
        } else {
            contexto.insertSubview(viewFinal.view, belowSubview: viewInicial.view)
        }
        let viewAtual = apresentarViewController ? viewFinal : viewInicial
        
        guard let tituloPreviewDaTelaFutura = viewAtual.view.viewWithTag(4) as? UILabel else { return }
        guard let descricaoPreviewDaTelaFutura = viewAtual.view.viewWithTag(5) as? UITextView else { return }
        guard let imagemPreviewDaTelaFutura = viewAtual.view.viewWithTag(6) as? UIImageView else { return }
        
        let tituloViewTelaAnterior = views[0] as! UILabel
        let descricaoViewTelaAnterior = views[1] as! UILabel
        let imagemViewTelaAnterior = views[2] as! UIImageView
        
        imagemPreviewDaTelaFutura.image = imagem
        tituloPreviewDaTelaFutura.isHidden = true
        descricaoPreviewDaTelaFutura.isHidden = true
        imagemPreviewDaTelaFutura.isHidden = true
        
        let frameTituloDeTransicao1 = CGRect(x: frames[0].minX, y: frames[0].minY, width: tituloPreviewDaTelaFutura.frame.width, height: frames[0].height)
        let frameTituloDeTransicao2 = CGRect(x: tituloPreviewDaTelaFutura.frame.minX, y: tituloPreviewDaTelaFutura.frame.maxY - 3, width: tituloPreviewDaTelaFutura.frame.width, height: tituloPreviewDaTelaFutura.frame.height)
        let frameDescricaoDeTransicao1 = CGRect(x: frames[1].minX, y: frames[1].minY, width: descricaoPreviewDaTelaFutura.frame.width, height: frames[1].height)
        let frameDescricaoDeTransicao2 = CGRect(x: descricaoPreviewDaTelaFutura.frame.minX, y: descricaoPreviewDaTelaFutura.frame.minY + 20, width: descricaoPreviewDaTelaFutura.frame.width, height: frames[1].height)
        let frameImagemDeTransicao1 = frames[2]
        let frameImagemDeTransicao2 = CGRect(x: imagemPreviewDaTelaFutura.frame.minX, y: imagemPreviewDaTelaFutura.frame.minY + 20, width: imagemPreviewDaTelaFutura.frame.width, height: imagemPreviewDaTelaFutura.frame.height)
        
        let tituloDeTransicao = apresentarViewController ? UILabel(frame: frameTituloDeTransicao1) : UILabel(frame: frameTituloDeTransicao2)
        tituloDeTransicao.textColor = UIColor(red: 0.13, green: 0.48, blue:0.88, alpha:1.0)
        tituloDeTransicao.font = UIFont(name: "Helvetica-Bold", size: 18)
        tituloDeTransicao.text = tituloViewTelaAnterior.text
//        tituloDeTransicao.text = textos[0]
        
        let descricaoDeTransicao = apresentarViewController ? UILabel(frame: frameDescricaoDeTransicao1) : UILabel(frame: frameDescricaoDeTransicao2)
        descricaoDeTransicao.textColor = UIColor(red: 0.43, green: 0.43, blue:0.43, alpha:1.0)
        descricaoDeTransicao.font = UIFont(name: "Helvetica-Light", size: 14)
        descricaoDeTransicao.numberOfLines = 0
        descricaoDeTransicao.lineBreakMode = .byWordWrapping
        descricaoDeTransicao.text = descricaoViewTelaAnterior.text
//        descricaoDeTransicao.text = textos[1]
        
        let imagemDeTransicao = apresentarViewController ? UIImageView(frame: frameImagemDeTransicao1) : UIImageView(frame: frameImagemDeTransicao2)
        imagemDeTransicao.backgroundColor = .systemGray4
        imagemDeTransicao.image = imagemPreviewDaTelaFutura.image
        
        contexto.addSubview(imagemDeTransicao) // ordem invertida para a imagem ficar abaixo dos labels
        contexto.addSubview(tituloDeTransicao)
        contexto.addSubview(descricaoDeTransicao)
        
        if apresentarViewController {
            viewAtual.view.alpha = 0.0
        } else {
            viewInicial.view.alpha = 1.0
        }
        tituloViewTelaAnterior.isHidden = true
        descricaoViewTelaAnterior.isHidden = true
        imagemViewTelaAnterior.isHidden = true
        
        viewAtual.view.layoutIfNeeded()
        
        UIView.animate(withDuration: duracao, animations: {
            tituloDeTransicao.frame = self.apresentarViewController ? frameTituloDeTransicao2 : frameTituloDeTransicao1
            descricaoDeTransicao.frame = self.apresentarViewController ? frameDescricaoDeTransicao2 : frameDescricaoDeTransicao1
            imagemDeTransicao.frame = self.apresentarViewController ? frameImagemDeTransicao2 : frameImagemDeTransicao1
            
            if self.apresentarViewController {
                viewAtual.view.alpha = 1.0
            } else {
                viewInicial.view.alpha = 0.0
            }
        }) { (_ ) in
            tituloDeTransicao.removeFromSuperview()
            descricaoDeTransicao.removeFromSuperview()
            imagemDeTransicao.removeFromSuperview()

            tituloViewTelaAnterior.isHidden = false
            descricaoViewTelaAnterior.isHidden = false
            imagemViewTelaAnterior.isHidden = false
            tituloPreviewDaTelaFutura.isHidden = false
            descricaoPreviewDaTelaFutura.isHidden = false
            imagemPreviewDaTelaFutura.isHidden = false
            transitionContext.completeTransition(true)
        }
    }
}
